import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.scss']
})
export class ClockComponent implements OnInit {

  s:number=0
  m:number =0
  h:number =0
  d?:Date
  ngOnInit(){
    setInterval(()=>{
      this.d = new Date()
      this.s = this.d.getSeconds()*6
      this.m=this.d.getMinutes()*6
      this.h=this.d.getHours() * 30 + Math.round(this.m / 12)
    }, 1000)
  }

  Today=Date.now()
}
